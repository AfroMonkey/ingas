<!DOCTYPE html>
<html>
  <body>
    <?php
      $database_path = $_GET['database_path'];
      $table = $_GET['table'];

      $db_handle  = new SQLite3($database_path) or die('Unable to open database');
      $db_handle->query("PRAGMA foreign_keys = ON");

      echo "<a href='control.php?database_path=$database_path&table=$table'>Volver</a>";

      echo "<h1>Tabla: $table</h1>";

      echo "<h2>Modificaciones</h2>";

      $sql = $db_handle->query("SELECT sql FROM sqlite_master WHERE sql LIKE '%REFERENCES%' AND name = '$table'")->fetchArray();
      $sql = $sql[0];
      $sql = substr($sql, strpos($sql, 'FOREIGN'));
      $foreigns = [];
      $references = [];
      $external = [];
      for ($i = 0; substr_count($sql, 'FOREIGN') ; $i++)
      {
        $sql = substr($sql, strpos($sql, 'KEY(') + 4);
        $foreigns[$i] = substr($sql, 0, strpos($sql, ')'));
        $sql = substr($sql, strpos($sql, 'REFERENCES') + 11);
        $references[$i] = substr($sql, 0, strpos($sql, '('));
        $sql = substr($sql, strpos($sql, '(') + 1);
        $external[$i] = substr($sql, 0, strpos($sql, ')'));
      }
      $values = false;
      if (isset($_POST['search']) || isset($_POST['update'])) {
        $where = "";
        $cols = [];
        $result = $db_handle->query("PRAGMA table_info($table)");
        for ($i = 0; $row = $result->fetchArray(); $i++) {
          if ($row[5] >= 1) {
            $where.="$row[1]={$_POST[$row[1]]} AND ";
          }
          $cols[$i] = $row[1];
        }
        $where = substr($where, 0, -5);
        $result = $db_handle->query("SELECT * FROM $table WHERE $where");
        $values = $result->fetchArray();
      }

      echo "<form action='update.php?database_path=$database_path&table=$table' method='POST'>";
        echo "<table>";
          $i = 0;
          $result = $db_handle->query("PRAGMA table_info($table)");
          while ($row = $result->fetchArray()) {
            echo "<tr>";
              echo "<td>$row[1]:</td>";
              echo "<td>";
              if (in_array($row[1], $foreigns) && $row[5] < 1) {
                $i = array_search($row[1], $foreigns);
                $keys = $db_handle->query("SELECT $external[$i] FROM $references[$i]");
                echo "<select name='$row[1]'";
                if (($values && isset($_POST['search']) && $row[5] >= 1) || ((!$values || !isset($_POST['search'])) && $row[5] < 1)) {
                  echo " disabled";
                }
                echo ">";
                  while ($key_values = $keys->fetchArray()) {
                    echo "<option value='$key_values[0]'";
                    if ($values[$i] == $key_values[0]) {
                      echo " selected";
                    }
                    echo ">$key_values[0]</option>";
                  }
                echo "</select>";
              } else {
                echo "<input name='$row[1]'";
                if (($values && isset($_POST['search']) && $row[5] >= 1) || ((!$values || !isset($_POST['search'])) && $row[5] < 1)) {
                  echo " readonly";
                }
                if (isset($_POST['search'])) {
                  echo " value='{$values[$i]}'";
                }
                echo "></input>";
              }
              echo "</td>";
            echo "</tr>";
            $i++;
          }
        echo "</table>";
        if (isset($_POST['search']) && $values) {
          echo "<button type='submit' name='update'>Modificar</button>";
        } else {
          echo "<button type='submit' name='search'>Buscar</button>";
        }
      echo "</form>";

      if (isset($_POST['search']) && !$values) {
        echo "<h3>Sin conicidencias</h3>";
      }

      if (isset($_POST['update'])) {
        $set = "";
        for ($i = 0; $i < count($values)/2; $i++) {
          if ($values[$i] != $_POST[$cols[$i]]) {
            $set.="$cols[$i]='{$_POST[$cols[$i]]}', ";
          }
        }
        if ($set == "") {
          echo "<h3>Sin cambios</h3>";
        } else {
          $set = substr($set, 0, -2);
          if ($db_handle->query("UPDATE $table SET $set WHERE $where")) {
            echo "<h3>Hecho</h3>";
          } else {
            echo "<h3>Error</h3>";
          }
        }
      }
      $db_handle->close();
    ?>
  </body>
</html>

PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS categoria(
  categoria_id INTEGER PRIMARY KEY,
  nombre TEXT,
  inicio NUMERIC,
  fin NUMERIC
);

CREATE TABLE IF NOT EXISTS gasto(
  registro_id INTEGER  PRIMARY KEY,
  proveedor_id INTEGER NOT NULL,
  dest_pago INTEGER,
  FOREIGN KEY(registro_id) REFERENCES registro(registro_id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(proveedor_id) REFERENCES persona(persona_id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(dest_pago) REFERENCES persona(persona_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ingreso(
  registro_id INTEGER PRIMARY KEY,
  origen_id INTEGER NOT NULL,
  FOREIGN KEY(registro_id) REFERENCES registro(registro_id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(origen_id) REFERENCES persona(persona_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS persona(
  persona_id INTEGER PRIMARY KEY,
  r_social TEXT,
  telefono NUMERIC
);

CREATE TABLE IF NOT EXISTS registro(
  registro_id INTEGER PRIMARY KEY,
  categoria_id INTEGER NOT NULL,
  usuario_id INTEGER NOT NULL,
  fecha DATE,
  concepto TEXT,
  monto REAL,
  referencia TEXT,
  descripcion TEXT,
  FOREIGN KEY(categoria_id) REFERENCES categoria(categoria_id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY(usuario_id) REFERENCES usuario(usuario_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS usuario(
  usuario_id INTEGER PRIMARY KEY,
  alias TEXT,
  contrasena TEXT,
  correo TEXT,
  FOREIGN KEY(usuario_id) REFERENCES persona(persona_id) ON UPDATE CASCADE ON DELETE CASCADE
);

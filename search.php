<!DOCTYPE html>
<html>
  <body>
    <?php
      $database_path = $_GET['database_path'];
      $table = $_GET['table'];

      $db_handle  = new SQLite3($database_path) or die('Unable to open database');
      $db_handle->query("PRAGMA foreign_keys = ON");

      echo "<a href='control.php?database_path=$database_path&table=$table'>Volver</a>";

      echo "<h1>Tabla: $table</h1>";

      echo "<h2>Consultas</h2>";

      echo "<form action='search.php?database_path=$database_path&table=$table' method='POST'>";
        echo "<table>";
          $i = 0;
          $result = $db_handle->query("PRAGMA table_info($table)");
          while ($row = $result->fetchArray()) {
            echo "<tr>";
              echo "<td>$row[1]:</td>";
              echo "<td><input name='$row[1]'></input></td>";
            echo "</tr>";
            $i++;
          }
        echo "</table>";
        echo "<button type='submit' name='search'>Buscar</button>";
      echo "</form>";

      if (isset($_POST['search'])) {
        $where = "";
        $result = $db_handle->query("PRAGMA table_info($table)");
        while ($row = $result->fetchArray()) {
          if ($_POST[$row[1]] != "") {
            $where.="$row[1]='{$_POST[$row[1]]}' AND ";
          }
        }
        $where = substr($where, 0, -5);
        if (!$where) {
          $where = "1 = 1";
        }
        $result = $db_handle->query("SELECT * FROM $table WHERE $where");
        $results = 0;
        echo "<h3>Resultados</h3>";
        while ($values = $result->fetchArray()) {
          $results++;
          echo "<form>";
            echo "<table>";
              $i = 0;
              $result_table_info = $db_handle->query("PRAGMA table_info($table)");
              while ($row = $result_table_info->fetchArray()) {
                echo "<tr>";
                  echo "<td>$row[1]:</td>";
                  echo "<td><input name='$row[1]' readonly value='{$values[$i]}'></input></td>";
                echo "</tr>";
                $i++;
              }
            echo "</table>";
          echo "</form>";
          echo "<br/>";
        }
        if ($results == 0) {
          echo "<p>Sin resultados</p>";
        }
      }
      $db_handle->close();
    ?>
  </body>
</html>

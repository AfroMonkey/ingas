<!DOCTYPE html>
<html>
  <body>
    <?php
      $database_path = $_GET['database_path'];
      $table = $_GET['table'];

      $db_handle  = new SQLite3($database_path) or die('Unable to open database');
      $db_handle->query("PRAGMA foreign_keys = ON");

      echo "<a href='control.php?database_path=$database_path&table=$table'>Volver</a>";

      echo "<h1>Tabla: $table</h1>";

      echo "<h2>Altas</h2>";

      $sql = $db_handle->query("SELECT sql FROM sqlite_master WHERE sql LIKE '%REFERENCES%' AND name = '$table'")->fetchArray();
      $sql = $sql[0];
      $sql = substr($sql, strpos($sql, 'FOREIGN'));
      $foreigns = [];
      $references = [];
      $external = [];
      for ($i = 0; substr_count($sql, 'FOREIGN') ; $i++)
      {
        $sql = substr($sql, strpos($sql, 'KEY(') + 4);
        $foreigns[$i] = substr($sql, 0, strpos($sql, ')'));
        $sql = substr($sql, strpos($sql, 'REFERENCES') + 11);
        $references[$i] = substr($sql, 0, strpos($sql, '('));
        $sql = substr($sql, strpos($sql, '(') + 1);
        $external[$i] = substr($sql, 0, strpos($sql, ')'));
      }

      $result = $db_handle->query("PRAGMA table_info($table)");

      echo "<form action='insert.php?database_path=$database_path&table=$table' method='POST'>";
        echo "<table>";
          while ($row = $result->fetchArray()) {
            echo "<tr>";
              echo "<td>$row[1]:</td>";
              echo "<td>";
                if (in_array($row[1], $foreigns)) {
                  $i = array_search($row[1], $foreigns);
                  $keys = $db_handle->query("SELECT $external[$i] FROM $references[$i]");
                  echo "<select name='$row[1]'>";
                    while ($key_values = $keys->fetchArray()) {
                      echo "<option value='$key_values[0]'>$key_values[0]</option>";
                    }
                  echo "</select>";
                } else {
                  echo "<input name='$row[1]'></input>";
                }
              echo "</td>";
            echo "</tr>";
          }
        echo "</table>";
        echo "<button type='submit' name='insert'>Alta</button>";
      echo "</form>";

      if(isset($_POST['insert'])) {
        $result = $db_handle->query("PRAGMA table_info($table)");
        $query = "INSERT INTO $table VALUES(";
        while ($row = $result->fetchArray()) {
            $query.= "'".$_POST[$row[1]]."', ";
        }
        $query = substr($query, 0, -2);
        $query.= ')';

        if($db_handle->query($query) === false) {
          echo "<h3>Error</h3>";
        } else {
          echo "<h3>Hecho</h3>";
        }
      }
      $db_handle->close();
    ?>
  </body>
</html>

<!DOCTYPE html>
<html>
  <body>
    <?php
      $database_path = $_GET['database_path'];
      $table = $_GET['table'];

      $db_handle  = new SQLite3($database_path) or die('Unable to open database');
      $db_handle->query("PRAGMA foreign_keys = ON");

      echo "<a href='control.php?database_path=$database_path&table=$table'>Volver</a>";

      echo "<h1>Tabla: $table</h1>";

      echo "<h2>Bajas</h2>";

      $result = $db_handle->query("PRAGMA table_info($table)");
      echo "<form action='delete.php?database_path=$database_path&table=$table' method='POST'>";
        echo "<table>";
          while ($row = $result->fetchArray()) {
            if ($row[5] >= 1) {
              echo "<tr>";
                echo "<td>$row[1]:</td>";
                echo "<td><input name='$row[1]'></input></td>";
              echo "</tr>";
            }
          }
        echo "</table>";
        echo "<button type='submit' name='delete'>Eliminar</button>";
      echo "</form>";

      if (isset($_POST['delete'])) {
        $result = $db_handle->query("PRAGMA table_info($table)");
        $query = "DELETE FROM $table WHERE ";
        while ($row = $result->fetchArray()) {
          if ($row[5] >= 1) {
            $query.="$row[1] = {$_POST[$row[1]]} AND ";
          }
        }
        $query = substr($query, 0, -5);
        $db_handle->query($query);
        if ($db_handle->changes() > 0) {
          echo "<h3>Hecho</h3>";
        } else {
          echo "<h3>Error</h3>";
        }
      }
      $db_handle->close();
    ?>
  </body>
</html>

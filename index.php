<!DOCTYPE html>
<html>
  <body>
    <?php
      $database_path = 'ingas.sqlite';

      $db_handle  = new SQLite3($database_path) or die('Unable to open database');
      $db_handle->query("PRAGMA foreign_keys = ON");

      echo "<h1>Base de datos: $database_path</h1>";

      echo "<h2>Tablas</h2>";

      $result = $db_handle->query("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name");
      echo "<ul>";
        while ($row = $result->fetchArray())
        {
          echo "<li><a href='control.php?database_path=$database_path&table={$row['name']}'>{$row['name']}</a></li>";
        }
      echo "</ul>";
      $db_handle->close();
    ?>
  </body>
</html>
